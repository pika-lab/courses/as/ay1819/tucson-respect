package it.unibo.sa.respect.loadb;

import alice.tuple.logic.LogicTuple;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

/**
 * Dummy Service Requestor class to show some 'adaptive' features related to
 * usage of uniform primitives. It probabilistically looks for available
 * services then issue a request to the Service Provider found.
 *
 * @author s.mariani@unibo.it
 */
public class ServiceRequestor extends AbstractTucsonAgent<EnhancedSyncACC> {

    /**
     * @param args no args expected.
     */
    public static void main(final String[] args) {
        try {
            new ServiceRequestor("requestor1", "default@localhost:20504").go();
            new ServiceRequestor("requestor2", "default@localhost:20504").go();
            new ServiceRequestor("requestor3", "default@localhost:20504").go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    private EnhancedSyncACC acc;
    private boolean die;
    private TucsonTupleCentreId tid;

    /**
     * @param aid  agent name
     * @param node node where to look for services
     * @throws TucsonInvalidAgentIdException if the chosen ID is not a valid TuCSoN agent ID
     */
    public ServiceRequestor(final String aid, final String node) throws TucsonInvalidAgentIdException {
        super(aid);
        die = false;
        try {
            say("I'm started.");
            tid = TucsonTupleCentreId.of(node);
        } catch (final TucsonInvalidTupleCentreIdException e) {
            say("Invalid tid given, killing myself...");
            die = true;
        }
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(getTucsonAgentId());

        return negAcc.playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        acc = getACC();

        TucsonOperation op;
        LogicTuple templ;
        LogicTuple service;
        LogicTuple req;
        final LogicTuple dieTuple = LogicTuple.parse("die(" + getTucsonAgentId() + ")");
        while (!die) {
            say("Checking termination...");
            op = acc.inp(tid, dieTuple, null);
            if (op.isResultSuccess()) {
                die = true;
                continue;
            }
            /*
             * Service search phase.
             */
            templ = LogicTuple.parse("ad(S)");
            say("Looking for services...");
            /*
             * Experiment alternative primitives and analyse different
             * behaviours.
             */
            op = acc.in(tid, templ, null);
            //                op = acc.urd(tid, templ, null);
            service = op.getLogicTupleResult();
            /*
             * Request submission phase.
             */
            say("Submitting request for service: %s", service.getArg(0));
            req = LogicTuple.parse("req(" + service.getArg(0) + ")");
            acc.out(tid, req, null);
            Thread.sleep(1000);
        }
        say("Someone killed me, bye!");
    }

}