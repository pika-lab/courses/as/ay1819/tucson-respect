package it.unibo.sa.respect.loadb;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Dummy Service Provider class to show some 'adaptive' features related to
 * usage of uniform primitives. TuCSoN Agent composed by 2 threads: main)
 * advertises its offered service and processes incoming requests taking them
 * from a private input queue; Receiver) waits for incoming requests and puts
 * them into main thread own queue.
 *
 * @author s.mariani@unibo.it
 */
public class ServiceProvider extends AbstractTucsonAgent<EnhancedSyncACC> {

    class Receiver extends Thread {

        @Override
        public void run() {
            LogicTuple res;
            TucsonOperation op;
            say("Waiting for requests...");
            try {
                final LogicTuple templ = LogicTuple.parse("req(" + service.getArg(0) + ")");
                while (!die) {
                    op = acc.in(tid, templ, null);
                    if (op.isResultSuccess()) {
                        nReqs++;
                        res = op.getLogicTupleResult();
                        say("Enqueuing request: " + res.toString());
                        /*
                         * We enqueue received request.
                         */
                        try {
                            inputQueue.add(res);
                        } catch (final IllegalStateException e) {
                            nLost++;
                            say("Queue is full, dropping request...");
                        }
                    }
                }
            } catch (final InvalidLogicTupleException e) {
                say("ERROR: Tuple is not an admissible Prolog term!");
            } catch (final TucsonOperationNotPossibleException e) {
                say("ERROR: Never seen this happen before *_*");
            } catch (final UnreachableNodeException e) {
                say("ERROR: Given TuCSoN Node is unreachable!");
            } catch (final OperationTimeOutException e) {
                say("ERROR: Endless timeout expired!");
            }
        }

        private void say(final String msg) {
            System.out.println("\t[Receiver]: " + msg);
        }
    }

    /**
     * @param args no args expected.
     */
    public static void main(final String[] args) {
        try {
            new ServiceProvider("provider1", "default@localhost:20504", 5000).go();
            new ServiceProvider("provider2", "default@localhost:20504", 3000).go();
            new ServiceProvider("provider3", "default@localhost:20504", 1000).go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    private static void serveRequest(final long time) throws InterruptedException {
        Thread.sleep(time);
    }

    private EnhancedSyncACC acc;
    private boolean die;
    /*
     * Decouples who performs the computation from who handles requests
     */
    private final BlockingQueue<LogicTuple> inputQueue;
    private LogicTuple service;
    /*
     * Models server performance in carrying out tasks
     */
    private final long serviceTime;
    TucsonTupleCentreId tid;
    private int nReqs;
    private int nLost;

    /**
     * @param aid     agent name
     * @param node    node where to advertise services
     * @param cpuTime to simulate computational power
     * @throws TucsonInvalidAgentIdException if the chosen ID is not a valid TuCSoN agent ID
     */
    public ServiceProvider(String aid, final String node, long cpuTime) throws TucsonInvalidAgentIdException {
        super(aid);
        die = false;
        try {
            tid = TucsonTupleCentreId.of(node);
            service = LogicTuple.parse("ad(" + aid + ")");
            say("I'm started.");
        } catch (final TucsonInvalidTupleCentreIdException e) {
            say("Invalid tid given, killing myself...");
            die = true;
        } catch (final InvalidLogicTupleException e) {
            say("Invalid aid given, killing myself...");
            die = true;
        }
        /*
         * The upper bound models server maximum computational load
         */
        inputQueue = new LinkedBlockingQueue<>(10);
        serviceTime = cpuTime;
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(getTucsonAgentId());

        return negAcc.playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        acc = getACC();

        new Receiver().start();
        TucsonOperation op;
        LogicTuple req;
        final LogicTuple dieTuple = LogicTuple.parse("die(" + getTucsonAgentId().getAgentId() + ")");
        while (!die) {
            say("Checking termination...");
            op = acc.inp(tid, dieTuple, null);
            if (op.isResultSuccess()) {
                die = true;
                continue;
            }
            /*
             * Service advertisement phase.
             */
            say("Advertising service: " + service.toString());
            acc.out(tid, service, null);
            /*
             * Request servicing phase.
             */
            boolean empty = true;
            try {
                say("Polling queue for requests...");
                while (empty) {
                    req = inputQueue.poll(1, TimeUnit.SECONDS);
                    if (req != null) {
                        empty = false;
                        say("Serving request: " + req.toString());
                        /*
                         * We simulate computational power of the Service
                         * Provider.
                         */
                        ServiceProvider.serveRequest(serviceTime);
                        /*
                         * Dummy 'positive feedback' mechanism.
                         */
                        say("Feedback to service: "
                                + service.toString());
                        acc.out(tid, service, null);
                    }
                }
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
            Thread.sleep(1000);
        }
        say("Someone killed me, bye!");
        printStats();
    }

    /**
     *
     */
    private void printStats() {
        say("My stats:");
        say("\t Total requests received: %d", nReqs);
        say("\t Requests served: %d", (nReqs - nLost));
        say("\t Requests lost: %d", nLost);
        say("\t Ratio served: %.2f%%", ratio(nReqs - nLost, nReqs));
        say("\t Ratio lost: %.2f%%", ratio(nLost, nReqs));
    }

    private double ratio(Number x, Number y) {
        return x.doubleValue() * 100d / y.doubleValue();
    }

}
