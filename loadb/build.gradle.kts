task<JavaExec>("runRequestor") {
    dependsOn("classes")
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.loadb.ServiceRequestor"
    standardInput = System.`in`

}

task<JavaExec>("runProvider") {
    dependsOn("classes")
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.loadb.ServiceProvider"
    standardInput = System.`in`

}
