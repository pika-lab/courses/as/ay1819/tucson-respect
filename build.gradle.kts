plugins {
    java
}

repositories {
    mavenCentral()
}

group = "it.unibo.as"
version = "1.0-SNAPSHOT"

val tucsonJars = listOf("cli", "client", "core", "inspector", "service").map { "${rootDir}/libs/$it.jar" }.toTypedArray()

subprojects {

    repositories {
        mavenCentral()
    }

    apply<JavaPlugin>()

    dependencies {
        implementation(files(*tucsonJars))
        implementation("it.unibo.alice.tuprolog", "tuprolog", "3.3.+")

        runtime("org.slf4j", "slf4j-api", "1.7.+")
        runtime("org.slf4j", "slf4j-nop", "1.7.+")
//        runtime("org.slf4j", "slf4j-simple", "1.7.+")

        testImplementation("junit", "junit", "4.12")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

val tuprologDir = File("tuprolog")
val tuprologZip = File(tuprologDir, "2p.zip")
val tuprologJar = File(tuprologDir, "bin/2p.jar")

dependencies {
    implementation(files(*tucsonJars))
    implementation("it.unibo.alice.tuprolog", "tuprolog", "3.3.+")

    runtime("org.slf4j", "slf4j-api", "1.7.+")
    runtime("org.slf4j", "slf4j-simple", "1.7.+")
}

task<JavaExec>("tucson") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tuplecentre.tucson.service.TucsonNodeService"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}

task<JavaExec>("inspector") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tuplecentre.tucson.introspection.tools.InspectorGUI"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}

task<JavaExec>("cli") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tuplecentre.tucson.service.tools.CommandLineInterpreter"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}
