# TuCSoN/ReSpecT exercises

### Important remarks

* Always remember to run:

    ```bash
    ./gradlew --stop
    ```

    after each demo is completed, in order to ensure all TuCSoN nodes are actually shutdown.

* You can always start a TuCSoN inspector by running:

    ```bash
    ./gradlew inspector
    ```

* You can always start a TuCSoN CLI by running:

    ```bash
    ./gradlew cli -Pport=<node port>
    ```

## Exercise 1 -- Rolling Dices (`dices` module)

Just run:

```bash
./gradlew runDices
```

## Exercise 2 -- Load Balancing (`loadb` module)


Run each command on a **different shell**:

1. Firstly, you must start a TuCSoN node:

    ```bash
    ./gradlew tucson
    ```

2. Then, you can start service providers

    ```bash
    ./gradlew runProvider
    ```

3. Then, you can start service requestors

    ```bash
    ./gradlew runRequestor
    ```

## Exercise 3 -- Swarm Intelligence (`swarm` module)

You can either start the whole demo in a single command:

```bash
./gradlew runSwarm
```

or run each stp on a **different shell**:

1. Firstly, you must start a 4 TuCSoN nodes:

    ```bash
    ./gradlew bootTopology
    ```

2. Then, you must configure them

    ```bash
    ./gradlew setupEnvironment
    ```

3. Finally, you can start the demo

    ```bash
    ./gradlew startGUI
    ```
