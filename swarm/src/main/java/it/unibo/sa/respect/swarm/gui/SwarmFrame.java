package it.unibo.sa.respect.swarm.gui;

import javax.swing.*;

/**
 * @author ste
 */
public class SwarmFrame extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param name
     */
    public SwarmFrame(final String name) {
        super(name);
        this.initComponents();
    }

    private void initComponents() {

        this.setSize(800, 700);
        this.setResizable(false);

    }

}
