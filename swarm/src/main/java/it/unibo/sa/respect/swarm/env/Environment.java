package it.unibo.sa.respect.swarm.env;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 */
public final class Environment {

    private static final String PATH = "./sa/lab/tucson/swarms/env/evaporation.rsp";

    private static TucsonAgentId me;
    private static NegotiationACC negAcc;
    private static EnhancedSyncACC acc;

    /**
     *
     */
    public static void config() throws OperationTimeOutException, TucsonInvalidAgentIdException, TucsonOperationNotPossibleException, UnreachableNodeException, InvalidLogicTupleException, TucsonInvalidTupleCentreIdException {

        acquireACC();

        final String spec = parseSpec();

        log("Configuring environment...");

        initAnthill(spec);

        initShortPath(spec);

        initLongPath(spec);

        initFoodSource();

        log("...environment configured");

    }

    private static void initFoodSource() throws TucsonInvalidTupleCentreIdException, InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        TucsonTupleCentreId tcid;
        log("Configuring <food>...");
        tcid = TucsonTupleCentreId.of("food", "localhost", "20504");
        acc.outAll(
                tcid,
                LogicTuple.parse("[nbr(short@localhost:20506),nbr(long2@localhost:20505)]"),
                null
        );
        acc.outAll(
                tcid,
                LogicTuple.parse("[anthill(short@localhost:20506),anthill(short@localhost:20506),anthill(long2@localhost:20505)]"),
                null
        );
        for (int i = 0; i < 1000; i++) {
            acc.out(tcid, LogicTuple.parse("food"), null);
        }
        log("...<food> configured");
    }

    private static void initLongPath(final String spec) throws TucsonInvalidTupleCentreIdException, InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        TucsonTupleCentreId tcid;

        log("Configuring <long1>...");
        tcid = TucsonTupleCentreId.of("long1", "localhost", "20507");
        acc.outAll(
                tcid,
                LogicTuple.parse("[nbr(anthill@localhost:20508),nbr(long2@localhost:20505)]"),
                null
        );
        acc.out(
                tcid,
                LogicTuple.parse("anthill(anthill@localhost:20508)"),
                null
        );
        acc.setS(tcid, spec, null);
        acc.out(tcid, LogicTuple.parse("'$start_evaporation'"), null);

        log("...<long1> configured");

        log("Configuring <long2>...");
        tcid = TucsonTupleCentreId.of("long2", "localhost", "20505");
        acc.outAll(tcid,
                LogicTuple.parse("[nbr(long1@localhost:20507),nbr(food@localhost:20504)]"),
                null
        );
        acc.out(tcid, LogicTuple.parse("anthill(long1@localhost:20507)"), null);
        acc.setS(tcid, spec, null);
        acc.out(tcid, LogicTuple.parse("'$start_evaporation'"), null);

        log("...<long2> configured");
    }

    private static void initShortPath(final String spec) throws TucsonInvalidTupleCentreIdException, InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {
        TucsonTupleCentreId tcid;

        log("Configuring <short>...");
        tcid = TucsonTupleCentreId.of("short", "localhost", "20506");
        acc.outAll(
                tcid,
                LogicTuple.parse("[nbr(anthill@localhost:20508), nbr(food@localhost:20504)]"),
                null);
        acc.out(tcid, LogicTuple.parse("anthill(anthill@localhost:20508)"), null);
        acc.setS(tcid, spec, null);
        acc.out(tcid, LogicTuple.parse("'$start_evaporation'"), null);

        log("...<short> configured");
    }

    private static void initAnthill(final String spec) throws TucsonInvalidTupleCentreIdException, InvalidLogicTupleException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {

        TucsonTupleCentreId tcid;

        log("Configuring <anthill>...");
        tcid = TucsonTupleCentreId.of("anthill", "localhost", "20508");
        acc.outAll(
                tcid,
                LogicTuple.parse("[nbr(short@localhost:20506), nbr(long1@localhost:20507)]"),
                null);
        acc.setS(tcid, spec, null);
        acc.out(tcid, LogicTuple.parse("'$start_evaporation'"), null);

        log("...<anthill> configured");
    }

    private static String parseSpec() {
        String spec = null;
        try {
            final BufferedReader r = new BufferedReader(new InputStreamReader(Environment.class.getResourceAsStream("evaporation.rsp")));
            return r.lines().collect(Collectors.joining("\n"));
        } catch (final NullPointerException e) {
            err("Cannot read spec from " + PATH);
            System.exit(-1);
        }
        return spec;
    }

    /**
     *
     */
    private static void acquireACC() throws TucsonInvalidAgentIdException, UnreachableNodeException, OperationTimeOutException, TucsonOperationNotPossibleException {

        me = TucsonAgentId.of("env");
        negAcc = TucsonMetaACC.getNegotiationContext(me);
        acc = negAcc.playDefaultRole();
    }

    private static void log(final String msg) {
        System.out.println("[ENV]: " + msg);
    }

    private static void err(final String msg) {
        System.err.println("[ENV]: " + msg);
    }

}
