package it.unibo.sa.respect.swarm.launchers;

import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;
import it.unibo.sa.respect.swarm.ants.Swarm;
import it.unibo.sa.respect.swarm.env.Environment;
import it.unibo.sa.respect.swarm.gui.GUI;
import it.unibo.sa.respect.swarm.utils.Topology;

/**
 * @author ste
 */
public class LaunchSwarmsScenario {

    /**
     * @param args
     */
    public static void main(final String[] args) throws UnreachableNodeException, TucsonOperationNotPossibleException, TucsonInvalidTupleCentreIdException, TucsonInvalidAgentIdException, InvalidLogicTupleException, OperationTimeOutException {

        System.out.println("Booting topology...");
        Topology.bootTopology();
        System.out.println("...topology boot");

        Environment.config();

        GUI.init();

        Swarm.release();

    }

}
