package it.unibo.sa.respect.swarm.launchers;

import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;
import it.unibo.sa.respect.swarm.env.Environment;

/**
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 */
public final class LaunchEnvironment {

    /**
     * @param args
     */
    public static void main(String[] args) throws UnreachableNodeException, TucsonOperationNotPossibleException, TucsonInvalidTupleCentreIdException, TucsonInvalidAgentIdException, InvalidLogicTupleException, OperationTimeOutException {

        Environment.config();
    }

}
