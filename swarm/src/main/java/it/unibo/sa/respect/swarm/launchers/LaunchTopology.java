package it.unibo.sa.respect.swarm.launchers;

import it.unibo.sa.respect.swarm.utils.Topology;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Stefano Mariani (mailto: s [dot] mariani [at] unibo [dot] it)
 */
public final class LaunchTopology {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Logger.getAnonymousLogger().log(Level.INFO, "Booting topology...");
        Topology.bootTopology();
        Logger.getAnonymousLogger().log(Level.INFO, "...topology boot");
    }

}
