/**
 *
 */
package it.unibo.sa.respect.swarm.ants;

import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;

/**
 * @author ste
 *
 */
public final class Swarm {

    private final static int ANTS = 10;

    public static void release() throws TucsonInvalidAgentIdException {
        for (int i = 0; i < ANTS; i++) {
            log("Releasing ant " + i + "...");
            new Ant("ant" + i, "localhost", 20508, "anthill").go();
            log("ant " + i + " released");
        }
    }

    private static void log(final String msg) {
        System.out.println("[SWARM]: " + msg);
    }

    private static void err(final String msg) {
        System.err.println("[SWARM]: " + msg);
    }
}
