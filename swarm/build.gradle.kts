task<JavaExec>("runSwarm") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchSwarmsScenario"
    standardInput = System.`in`

}

task<JavaExec>("bootTopology") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchTopology"
    standardInput = System.`in`

}

task<JavaExec>("setupEnvironment") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchEnvironment"
    standardInput = System.`in`

}

task<JavaExec>("startGUI") {
    dependsOn("classes")
//    group = "exercise2"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.swarm.launchers.LaunchSwarmWithGUI"
    standardInput = System.`in`

}