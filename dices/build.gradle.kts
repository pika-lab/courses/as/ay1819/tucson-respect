task<JavaExec>("makeDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dices.DiceBuilder"
    standardInput = System.`in`
}

task<JavaExec>("cleanDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dices.DiceCleaner"
    standardInput = System.`in`
}

task<JavaExec>("rollDice") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dices.DicePlayer"
}

task<JavaExec>("stopDicePlayer") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.sa.respect.dices.DiceStopper"
}
