package it.unibo.sa.respect.dices;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.tucson.network.exceptions.DialogInitializationException;

import java.util.HashMap;
import java.util.Map;

public class DicePlayer extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        new DicePlayer("roller") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }

    private boolean stop;
    private TucsonTupleCentreId tcid;
    private Map<Integer, Integer> outcomes;

    public DicePlayer(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        this.tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
        this.stop = false;
        this.outcomes = new HashMap<>();
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(getTucsonAgentId());
        return negAcc.playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        final EnhancedSyncACC acc = getACC();
        final LogicTuple dieTuple = LogicTuple.of("stop", TupleArgument.of(this.getTucsonAgentId().getLocalName()));
        TucsonOperation op;
        LogicTuple template;
        int face;
        Integer nTimes = 1;

        while (!this.stop) {
            say("Checking termination...");
            op = acc.inp(this.tcid, dieTuple, null);
            if (op.isResultSuccess()) {
                this.stop = true;
                continue;
            }

            template = LogicTuple.of("face", TupleArgument.var());
            this.say("Rolling dice...");
            op = acc.rd(this.tcid, template, Long.MAX_VALUE);
            // this.say("Rolling dice uniformly...");
            // op = acc.urd(this.tcid, template, Long.MAX_VALUE);
            if (op.isResultSuccess()) {
                face = op.getLogicTupleResult().getArg(0).intValue();
                this.say("...they see me rollin', they hatin': " + face);
                nTimes = this.outcomes.get(face);
                if (nTimes == null) {
                    this.outcomes.put(face, 1);
                } else {
                    this.outcomes.put(face, ++nTimes);
                }
            }

            printStats();
            Thread.sleep(500);
        }

        this.say("Someone killed me, bye!");
        printStats(true);
    }

    private void printStats() {
        printStats(false);
    }

    private void printStats(boolean end) {
        if (end) {
            this.say("Final outcomes 'till now:");
        } else {
            this.say("Outcomes 'till now:");
        }
        Integer sum = this.outcomes.entrySet().stream().mapToInt(Map.Entry::getValue).sum();
        for (Integer i : this.outcomes.keySet()) {
            Integer t = this.outcomes.get(i);
            this.say("\tFace %d drawn %d of %d times (ratio: %.2f%%)", i, t, sum, ratio(t, sum));
        }
    }

    private double ratio(Number x, Number y) {
        return x.doubleValue() * 100d / y.doubleValue();
    }

}
