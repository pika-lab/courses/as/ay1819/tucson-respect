package it.unibo.sa.respect.dices;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.tucson.network.exceptions.DialogInitializationException;

public class DiceStopper extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        new DiceStopper("stopper") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }

    private TucsonTupleCentreId tcid;

    public DiceStopper(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        this.tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(getTucsonAgentId());
        return negAcc.playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        final EnhancedSyncACC acc = getACC();
        final LogicTuple dieTuple = LogicTuple.of("stop", TupleArgument.of("roller"));

        TucsonOperation op = acc.out(this.tcid, dieTuple, null);

        if (op.isResultSuccess()) {
            say("Roller stopped");
        } else {
            say("WHAAAT?");
        }
    }
}
