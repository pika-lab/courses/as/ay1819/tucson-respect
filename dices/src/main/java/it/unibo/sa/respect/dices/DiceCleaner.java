package it.unibo.sa.respect.dices;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.TupleArgument;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.AbstractTucsonAgent;
import alice.tuplecentre.tucson.api.TucsonAgentId;
import alice.tuplecentre.tucson.api.TucsonMetaACC;
import alice.tuplecentre.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.tucson.network.exceptions.DialogInitializationException;

public class DiceCleaner extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        new DiceCleaner("cleaner") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }

    private boolean stop;
    private TucsonTupleCentreId tcid;

    public DiceCleaner(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        this.tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(getTucsonAgentId());
        return negAcc.playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        final EnhancedSyncACC acc = getACC();
        LogicTuple template = LogicTuple.of("face", TupleArgument.var());

        acc.inAll(this.tcid, template, null);
    }

}
