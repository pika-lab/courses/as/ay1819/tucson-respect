package it.unibo.sa.respect.dices;

import alice.tuple.logic.LogicTuple;
import alice.tuple.logic.exceptions.InvalidLogicTupleException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.tucson.api.*;
import alice.tuplecentre.tucson.api.acc.EnhancedSyncACC;
import alice.tuplecentre.tucson.api.acc.NegotiationACC;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tuplecentre.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.tucson.network.exceptions.DialogInitializationException;

public class DiceBuilder extends AbstractTucsonAgent<EnhancedSyncACC> {

    public static void main(final String[] args) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        new DiceBuilder("builder") {
            @Override
            protected void onTermination() {
                System.exit(0);
            }
        }.go();
    }

    private TucsonTupleCentreId tcid;

    public DiceBuilder(String aid) throws TucsonInvalidAgentIdException, TucsonInvalidTupleCentreIdException {
        super(aid);
        this.tcid = TucsonTupleCentreId.of("dice", "localhost", "20504");
    }

    @Override
    protected EnhancedSyncACC retrieveACC(TucsonAgentId tucsonAgentId, String s, int i) throws Exception {
        final NegotiationACC negAcc = TucsonMetaACC.getNegotiationContext(getTucsonAgentId());
        return negAcc.playDefaultRole();
    }

    @Override
    protected void main() throws Exception {
        final EnhancedSyncACC acc = getACC();
        TucsonOperation op = acc.outAll(TucsonTupleCentreId.of("dice", "localhost", "20504"),
                LogicTuple.parse("[face(1),face(2),face(3),face(4),face(5),face(6)]"),
                Long.MAX_VALUE);

        if (op.isResultSuccess()) {
            say("Dice configured");
        } else {
            say("WHAAAT?");
        }
    }
}
